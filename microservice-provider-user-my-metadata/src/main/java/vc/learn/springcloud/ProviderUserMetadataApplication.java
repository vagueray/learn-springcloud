package vc.learn.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Created on 2018-06-30 下午7:58.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@EnableEurekaClient
@SpringBootApplication
public class ProviderUserMetadataApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProviderUserMetadataApplication.class,args);
    }
}
