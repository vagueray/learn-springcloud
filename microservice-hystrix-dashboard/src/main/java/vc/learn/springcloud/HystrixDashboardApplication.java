package vc.learn.springcloud;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.cloud.netflix.hystrix.dashboard.*;

/**
 * Created on 2018-07-08 下午5:19.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@EnableHystrixDashboard
@SpringBootApplication
public class HystrixDashboardApplication {
    public static void main(String[] args) {
        SpringApplication.run(HystrixDashboardApplication.class, args);
    }
}
