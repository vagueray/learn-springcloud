package vc.learn.springcloud;

import org.springframework.cloud.netflix.feign.*;
import org.springframework.web.bind.annotation.*;
import vc.learn.springcloud.dto.User;

/**
 * Created on 2018-07-08 上午11:16.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
public interface UserFeignClient {

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User findById(@PathVariable("id") Long id);
}
