package vc.learn.springcloud;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.cloud.client.circuitbreaker.*;
import org.springframework.cloud.client.loadbalancer.*;
import org.springframework.context.annotation.*;
import org.springframework.web.client.*;

/**
 * Created on 2018-06-30 下午9:04.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@EnableCircuitBreaker
@SpringBootApplication
public class ConsumerMovieRibbonHystrixTurbineMqApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerMovieRibbonHystrixTurbineMqApplication.class,args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
