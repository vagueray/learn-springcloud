package vc.learn.springcloud;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.cloud.netflix.eureka.*;

/**
 * Created on 2018-06-30 下午7:58.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@EnableEurekaClient
@SpringBootApplication
public class ProviderUserWithAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProviderUserWithAuthApplication.class,args);
    }
}
