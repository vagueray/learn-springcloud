package vc.learn.springcloud.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;
import vc.learn.springcloud.entity.User;

/**
 * Created on 2018-06-30 下午7:48.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@Repository
public interface UserRepository extends JpaRepository<User,Long> {
}
