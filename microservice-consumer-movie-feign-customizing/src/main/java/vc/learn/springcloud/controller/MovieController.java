package vc.learn.springcloud.controller;

import org.springframework.web.bind.annotation.*;
import vc.learn.springcloud.UserFeignClient;
import vc.learn.springcloud.dto.User;

/**
 * Created on 2018-06-30 下午9:07.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@RestController
public class MovieController {

    private UserFeignClient userFeignClient;

    public MovieController(UserFeignClient userFeignClient) {
        this.userFeignClient = userFeignClient;
    }

    @GetMapping("/user/{id}")
    public User findById(@PathVariable Long id) {
        return this.userFeignClient.findById(id);
    }
}
