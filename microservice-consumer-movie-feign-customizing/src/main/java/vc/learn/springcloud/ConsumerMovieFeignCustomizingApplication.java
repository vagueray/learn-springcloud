package vc.learn.springcloud;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.cloud.client.discovery.*;
import org.springframework.cloud.netflix.feign.*;
import org.springframework.context.annotation.*;
import org.springframework.web.client.*;
import vc.learn.springcloud.config.FeignConfiguration;

/**
 * Created on 2018-06-30 下午9:04.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication(exclude = FeignConfiguration.class)
public class ConsumerMovieFeignCustomizingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerMovieFeignCustomizingApplication.class,args);
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
