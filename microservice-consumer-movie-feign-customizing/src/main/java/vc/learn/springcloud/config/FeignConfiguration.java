package vc.learn.springcloud.config;

import feign.Contract;
import org.springframework.context.annotation.*;

/**
 * Created on 2018-07-08 上午11:32.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@Configuration
public class FeignConfiguration {

    @Bean
    public Contract feignContract(){
        return new feign.Contract.Default();
    }
}
