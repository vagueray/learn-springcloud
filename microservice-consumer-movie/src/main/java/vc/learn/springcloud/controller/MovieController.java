package vc.learn.springcloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import vc.learn.springcloud.dto.User;

/**
 * Created on 2018-06-30 下午9:07.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@RestController
public class MovieController {

    private RestTemplate restTemplate;

    public MovieController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/user/{id}")
    public User findById(@PathVariable Long id) {
        return this.restTemplate.getForObject("http://microservice-provider-user/" + id, User.class);
    }
}
