package vc.learn.springcloud.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created on 2018-06-30 下午9:05.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@Data
public class User {
    private Long id;
    private String username;
    private String name;
    private Integer age;
    private BigDecimal balance;
}
