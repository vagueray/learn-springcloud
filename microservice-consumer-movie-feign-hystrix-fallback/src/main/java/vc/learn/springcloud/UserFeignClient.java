package vc.learn.springcloud;

import org.springframework.cloud.netflix.feign.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import vc.learn.springcloud.dto.User;

/**
 * Created on 2018-07-08 上午11:16.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@FeignClient(name = "microservice-provider-user",
        fallback = FeignClientFallback.class)
public interface UserFeignClient {

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    User findById(@PathVariable("id") Long id);
}

@Component
class FeignClientFallback implements UserFeignClient {

    @Override
    public User findById(Long id) {
        User user = new User();
        user.setId(-1L);
        user.setName("默认用户");
        return user;
    }
}
