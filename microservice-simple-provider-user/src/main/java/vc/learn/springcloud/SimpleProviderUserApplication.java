package vc.learn.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created on 2018-06-30 下午7:58.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@SpringBootApplication
public class SimpleProviderUserApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimpleProviderUserApplication.class,args);
    }
}
