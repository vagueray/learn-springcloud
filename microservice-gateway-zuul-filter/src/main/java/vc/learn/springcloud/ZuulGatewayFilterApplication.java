package vc.learn.springcloud;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.cloud.netflix.zuul.*;
import org.springframework.context.annotation.*;
import vc.learn.springcloud.filter.PreRequestLogFilter;

/**
 * Created on 2018-08-18 下午3:13.
 *
 * @author <a href="mailto:xiaolei.fu@hotmail.com">xiaolei.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@EnableZuulProxy
@SpringBootApplication
public class ZuulGatewayFilterApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZuulGatewayFilterApplication.class,args);
    }

    @Bean
    public PreRequestLogFilter preRequestLogFilter(){
        return new PreRequestLogFilter();
    }
}
