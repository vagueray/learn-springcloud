package vc.learn.springcloud.dto;

import lombok.Data;

import java.math.*;

/**
 * Created on 2018-08-18 下午4:17.
 *
 * @author <a href="mailto:xiaolei.fu@hotmail.com">xiaolei.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@Data
public class User {
    private Long id;
    private String username;
    private String name;
    private Integer age;
    private BigDecimal balance;
}
