package vc.learn.springcloud;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.cloud.client.loadbalancer.*;
import org.springframework.cloud.netflix.zuul.*;
import org.springframework.context.annotation.*;
import org.springframework.web.client.*;

/**
 * Created on 2018-08-18 下午3:13.
 *
 * @author <a href="mailto:xiaolei.fu@hotmail.com">xiaolei.fu</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@EnableZuulProxy
@SpringBootApplication
public class ZuulGatewayAggregationApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZuulGatewayAggregationApplication.class,args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
